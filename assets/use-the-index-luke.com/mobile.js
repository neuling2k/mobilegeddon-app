$(function() {
    if($(window).width()<=993){
      $content = $("#content");
      $content.css( { marginLeft: 0, marginRight: 0, width: "auto" });
/*
        headerBackgroundImage = $("#block-block-3 a").css("background-image").match(/\(([^\)]+)/)[1];
*/
       var blogLink = $("#block-blog-0 h2 a");
      //hide everything we don't need
        $(".navbar").removeClass("navbar");
        $("div[style*='width']").each(function(index,element){
            if(element.style.width!="auto"){
                if($(element).width()>320){
                    $(element).hide();
                } 
            }
        });
/*
      $(".region-sidebar-first, #page-closure, .share, .navbar3, #header, #breadcrumb-wrapper, #block-block-216, iframe, #block-block-103").hide();
*/
        $("link[href='//use-the-index-luke.com//files/css/css_bf882526b4b0279f99d7460d38a46c2a.css']").remove();
        $("input[type='submit']").addClass("btn btn-default form-group")
        $("#page-wrapper").addClass("container");
        $("#page-wrapper").css({marginTop:"60px"});
        $("input[type='text']").addClass("form-control form-group");
        $("img").addClass("img-responsive");
        $("#block-lang_dropdown-0").addClass("pull-right");
        $("#block-lang_dropdown-0").insertBefore($("#main-title"));

        //create bootstrap navbar
        //create navbar on top
        var navbar = document.createElement("nav");
        navbar.className="navbar navbar-default navbar-fixed-top";
        navbar.style.backgroundColor="#1A4E8A";
        navbar.style.boxShadow="0px 5px 5px 0px rgba(0,0,0,0.5)";
        var containerDiv = document.createElement("div");
        containerDiv.className="container";
        var navbarHeader = document.createElement("div");
        navbarHeader.className="navbar-header";
        var brand = document.createElement("span")
        brand.innerHTML = "Use the Index, Luke";
        brand.className = "navbar-brand";
        navbarHeader.appendChild(brand);
        var button = document.createElement("button");
        button.className="navbar-toggle";
        button.innerHTML = "<span class='sr-only'>Toggle navigation</span> <span class='icon-bar'></span><span class='icon-bar'></span><span class='icon-bar'></span>";
        button.setAttribute("data-toggle","collapse");
        button.setAttribute("data-target","navbar");
        var menu = document.createElement("div");
        menu.setAttribute("id","navbar");
        menu.className="navbar-collapse collapse";
        menu.style.maxHeight="600px";
        menu.style.overflowX="hidden";
        menu.style.overflowY="auto";
        var ul = document.createElement("ul");
        ul.className="nav navbar-nav";
        //fill navbar with sidebar content
        $($('.book-block-menu .menu')[0]).children("li").each(function(index,element){
            if($(element).children("ul").length>0){
                var tempElement = $(element).clone();
                tempElement.children("ul").remove();
                $(tempElement[0]).addClass("active");
                tempElement.appendTo(ul);
            }
            else{
                ul.appendChild(element);
            }
        });
        var tempLi = document.createElement("li");
        tempLi.appendChild(blogLink[0]);
        ul.appendChild(tempLi);
        menu.appendChild(ul);
        $("#main-title").after($('.book-block-menu .menu ul'));
        navbar.appendChild(containerDiv);
        navbarHeader.appendChild(button);
        containerDiv.appendChild(navbarHeader);
        containerDiv.appendChild(menu);
        $("body")[0].appendChild(navbar);
/*        if(headerBackgroundImage){
            $("nav.navbar-default").css({ minHeight: 57, background: "#1A4E8A url(" + headerBackgroundImage + ") no-repeat left top", backgroundSize: "120%" });
        }*/
            // Navbar and dropdowns
        var toggle = document.getElementsByClassName('navbar-toggle')[0],
            collapse = document.getElementsByClassName('navbar-collapse')[0],
            dropdowns = document.getElementsByClassName('dropdown');;

        // Toggle if navbar menu is open or closed
        function toggleMenu() {
            collapse.classList.toggle('collapse');
            collapse.classList.toggle('in');
        }
        // Close all dropdown menus
        function closeMenus() {
            for (var j = 0; j < dropdowns.length; j++) {
            dropdowns[j].getElementsByClassName('dropdown-toggle')[0].classList.remove('dropdown-open');
            dropdowns[j].classList.remove('open');
            }
        }
        // Add click handling to dropdowns
        for (var i = 0; i < dropdowns.length; i++) {
            dropdowns[i].addEventListener('click', function() {
                var open = this.classList.contains('open');
                closeMenus();
                if (!open) {
                this.getElementsByClassName('dropdown-toggle')[0].classList.toggle('dropdown-open');
                this.classList.toggle('open');
                }
            });
        }
        // Close dropdowns when screen becomes big enough to switch to open by hover
        toggle.addEventListener('click', toggleMenu, false);
    }
});