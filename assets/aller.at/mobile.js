document.addEventListener("DOMContentLoaded", function(event) {
console.log("Mobilegeddon.js loaded");
  if($(window).width()<993){
  	//inject bootstrap css
  	var link = document.createElement("link");
	link.href = "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css";
	link.type = "text/css";
	link.rel = "stylesheet";
	document.getElementsByTagName("head")[0].appendChild(link);
	//inject viewport
	var viewPortTag=document.createElement('meta');
	viewPortTag.id="viewport";
	viewPortTag.name = "viewport";
	viewPortTag.content = "width=device-width, initial-scale=1";
	$('head')[0].appendChild(viewPortTag);
	//enable responsive sizing
	$("#footer2")[0].style.width="auto";
	$("#footer_line")[0].style.width="auto";
	$(".content-box")[0].style.width="auto";
	$("#core_container2")[0].style.width="auto";
	$("#core_right")[0].style.width="auto";
	//create navbar on top
	var navbar = document.createElement("nav");
	navbar.className="navbar navbar-default navbar-fixed-top";
	navbar.style.backgroundColor="#FFECD5";
	navbar.style.boxShadow="0px 5px 5px 0px rgba(0,0,0,0.5)";
	var containerDiv = document.createElement("div");
	containerDiv.className="container";
	var navbarHeader = document.createElement("div");
	navbarHeader.className="navbar-header";
	var button = document.createElement("button");
	button.className="navbar-toggle";
	button.innerHTML = "<span class='sr-only'>Toggle navigation</span> <span class='icon-bar'></span><span class='icon-bar'></span><span class='icon-bar'></span>";
	button.setAttribute("data-toggle","collapse");
	button.setAttribute("data-target","navbar");
	var menu = document.createElement("div");
	menu.setAttribute("id","navbar");
	menu.className="navbar-collapse collapse";
	menu.style.maxHeight="600px";
	menu.style.overflowX="hidden";
	menu.style.overflowY="auto";
	var ul = document.createElement("ul");
	ul.className="nav navbar-nav";
	var dropdown = document.createElement("li");
	dropdown.className="dropdown";
	var dropDownLink = document.createElement("a");
	dropDownLink.setAttribute("href","#");
	dropDownLink.setAttribute("role","button");
	dropDownLink.className="dropdown-toggle";
	dropDownLink.innerHTML=document.getElementById("highlight").innerHTML+'<span class="caret"></span>';
	var dropDownMenu = document.createElement("ul");
	dropDownMenu.className="dropdown-menu";
	dropDownMenu.setAttribute("role","menu");
	dropdown.appendChild(dropDownLink);
	//fill new navbar with content from current sidebar
	var menuItems = $("#menuepunkte");
	$("#menuepunkte").children("a").each(function(index,element){
		if($(element).children("img").length<=0){
			var li = document.createElement("li");
			li.appendChild(element);
			dropDownMenu.appendChild(li);
		}
	});
	dropdown.appendChild(dropDownMenu);
	//fill navbar with content from top navbar
	var topMenuItems = document.getElementById("navcontainer").getElementsByTagName("li");
	$("#navcontainer li").each(function(index,element){
		ul.appendChild(element);
	});
	ul.appendChild(dropdown);
	//remove header
	document.getElementById("core_header").remove();
	menu.appendChild(ul);
	navbar.appendChild(containerDiv);
	navbarHeader.appendChild(button);
	containerDiv.appendChild(navbarHeader);
	containerDiv.appendChild(menu);
	document.getElementsByTagName("body")[0].appendChild(navbar);
	//we are done with the sidebar so lets remove it
	document.getElementsByTagName("td")[0].remove();
	document.getElementsByTagName("td")[0].remove();
	document.getElementById("container").style.width= "auto";
	document.getElementById("container").style.marginTop= "50px";
	document.getElementById("container").className= "container";
	document.getElementById("core_right").style.background="none";
	document.getElementById("core_right").style.backgroundColor="#FFECD5";
	document.getElementsByTagName("img")[0].removeAttribute("align");
	document.getElementsByTagName("img")[0].style.display = "block";
	// Navbar and dropdowns
	var toggle = document.getElementsByClassName('navbar-toggle')[0],
	    collapse = document.getElementsByClassName('navbar-collapse')[0],
	    dropdowns = document.getElementsByClassName('dropdown');;

	// Toggle if navbar menu is open or closed
	function toggleMenu() {
	    collapse.classList.toggle('collapse');
	    collapse.classList.toggle('in');
	}
	// Close all dropdown menus
	function closeMenus() {
	    for (var j = 0; j < dropdowns.length; j++) {
		dropdowns[j].getElementsByClassName('dropdown-toggle')[0].classList.remove('dropdown-open');
		dropdowns[j].classList.remove('open');
	    }
	}
	// Add click handling to dropdowns
	for (var i = 0; i < dropdowns.length; i++) {
	    dropdowns[i].addEventListener('click', function() {
		    var open = this.classList.contains('open');
		    closeMenus();
		    if (!open) {
			this.getElementsByClassName('dropdown-toggle')[0].classList.toggle('dropdown-open');
			this.classList.toggle('open');
		    }
	    });
	}
	// Close dropdowns when screen becomes big enough to switch to open by hover
	toggle.addEventListener('click', toggleMenu, false);
  }
});
