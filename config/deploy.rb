set :application, "mobilegeddon.at"
set :repo_url,    "git@bitbucket.org:neuling2k/mobilegeddon-app.git"

# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

# set :scm, :git

# set :format, :pretty
# set :log_level, :debug
# set :pty, true

# set :linked_files, %w{config/config.yml}

set :linked_files, %w{aws-keys.json}
set :linked_dirs, %w{log node_modules}

# set :default_env, { path: "/opt/ruby/bin:$PATH" }
# set :keep_releases, 5

namespace :deploy do
  
  before :updated, :rebuild_npm do
    on roles(:app) do
      execute "cd #{release_path} && npm install"
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), wait: 5 do
      # Your restart mechanism here, for example:
      execute :mkdir, '-p', release_path.join('tmp')
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :finishing, 'deploy:cleanup'
  after :publishing, :restart
end