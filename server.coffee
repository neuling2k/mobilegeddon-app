restify = require "restify"
needle  = require "needle"
cheerio = require "cheerio"
URI     = require "url"
fs      = require "fs"
mime    = require "mime-types"
etag    = require "etag"

server  = restify.createServer()
server.use restify.queryParser()

LIVERELOAD_ENABLED = process.argv.indexOf("livereload") != -1
NODE_ENV           = process.env.NODE_ENV || "development"

asset = (req, res, next) ->
  hostPath    = "assets/#{req.params.host}"
  vendorsPath = "#{hostPath}/vendors"
  file        = "#{hostPath}/#{req.params.asset}"
  
  vendorsData = []
  
  if fileMatch = req.params.asset.match /\.(js|css)$/
    extension = fileMatch?[1]
    if fs.existsSync(vendorsPath) && vendorFiles = fs.readdirSync(vendorsPath)
      for vendorFile in vendorFiles
        continue if vendorFile.split(".").reverse()[0] != extension
        vendorsData.push fs.readFileSync("#{vendorsPath}/#{vendorFile}", data: "string")

  if fs.existsSync(file) && data = fs.readFileSync(file, data: "string")
    data = vendorsData.join() + data
    res.writeHead 200, { "Content-type": mime.lookup(file), "ETag": etag(data) }
    res.write data
    
  res.end()
  
root = (req, res, next) ->
  url = URI.parse(req.params.site || "")
  
  return res.end() if !url.hostname
  
  needle.get url.href, follow_max: 5, (error, response) ->
    $ = cheerio.load response.body
    
    $("head").append "<base href='//#{url.hostname}'>"
    
    $("script[src*='cdn.mobilegeddon.at'], link[href*='cdn.mobilegeddon.at']").remove()
    
    $("script").each (index, elem) -> 
      if elem.attribs.src && src = URI.parse(elem.attribs.src)
        elem.attribs.src = "//#{src.hostname || url.hostname}/#{src.path}"
    
    $("link").each (index, elem) -> 
      if elem.attribs.href && href = URI.parse(elem.attribs.href)
        elem.attribs.href = "//#{href.hostname || url.hostname}/#{href.path}"
        
    $("a").each (index, elem) ->
      if elem.attribs.href && href = URI.parse(elem.attribs.href)
        elem.attribs.href = "http://#{req.headers.host}/?site=#{url.protocol}//" + "#{url.hostname}/#{href.path ? ''}#{href.hash ? ''}".replace(/\/\//, "/")
    
    if NODE_ENV == "development"
      $("body").append "<link rel='stylesheet' type='text/css' href='http://#{req.headers.host}/assets/#{url.hostname}/mobile.css'>" if fs.existsSync("assets/#{url.hostname}/mobile.css")
      $("body").append "<script src='http://#{req.headers.host}/assets/#{url.hostname}/mobile.js'>" if fs.existsSync("assets/#{url.hostname}/mobile.js")
    else
      $("body").append "<link rel='stylesheet' type='text/css' href='//cdn.mobilegeddon.at.s3-website-us-west-2.amazonaws.com/#{url.hostname}/mobile.css'>"
      $("body").append "<script src='//cdn.mobilegeddon.at.s3-website-us-west-2.amazonaws.com/#{url.hostname}/mobile.js'>"
  
    $("head").append "<script src='http://livejs.com/live.js'>" if LIVERELOAD_ENABLED
    
    res.writeHead 200, { "Content-type": "text/html", "ETag": etag($.html()) }
    res.write $.html()
    res.end()

server.get "/assets/:host/:asset", asset
server.head "/assets/:host/:asset", asset

server.get "/(.*)", root
server.head "/(.*)", root
  
server.listen 8080, ->
  console.log "Mobilegeddon-Server is running on http://localhost:8080 (livereload enabled: #{LIVERELOAD_ENABLED})"