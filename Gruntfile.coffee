module.exports = (grunt) ->
    
  grunt.initConfig
    pkg: grunt.file.readJSON "package.json"
    
    clean: ["assets/<%= grunt.option('host') %>/deploy"]
    
    cleanempty:
      src: ["assets/<%= grunt.option('host') %>/deploy/*"],
            
    cssmin:
      options:
        shorthandCompacting: false
        roundingPrecision: -1
      target:
        files:
          "assets/<%= grunt.option('host') %>/deploy/mobile.css": ["assets/<%= grunt.option('host') %>/vendors/*.css", "assets/<%= grunt.option('host') %>/mobile.css"]
        
    uglify:
      options:
        mangle: true
      dist:
        files:
          "assets/<%= grunt.option('host') %>/deploy/mobile.js": ["assets/<%= grunt.option('host') %>/vendors/*.js", "assets/<%= grunt.option('host') %>/mobile.js"]
    
    aws: grunt.file.readJSON('aws-keys.json')
    
    aws_s3:
      options:
        accessKeyId: '<%= aws.AWSAccessKeyId %>'
        secretAccessKey: '<%= aws.AWSSecretKey %>'
        region: 'us-west-2'
        uploadConcurrency: 5
        downloadConcurrency: 5
      
      clean:
        options:
          bucket: 'cdn.mobilegeddon.at'
          debug: true
          
        files: [
          { dest: "<%= grunt.option('host') %>", action: 'delete' }
        ]
      
      upload:
        options:
          bucket: 'cdn.mobilegeddon.at'
            
        files: [
          { expand: true, cwd: "assets/<%= grunt.option('host') %>/deploy", src: ["*"], dest: "<%= grunt.option('host') %>" }
        ]

  grunt.loadNpmTasks "grunt-aws-s3"
  grunt.loadNpmTasks "grunt-notify"
  grunt.loadNpmTasks "grunt-contrib-clean"
  grunt.loadNpmTasks "grunt-contrib-cssmin"
  grunt.loadNpmTasks "grunt-contrib-uglify"
  grunt.loadNpmTasks "grunt-cleanempty"
  
  grunt.registerTask "deploy", ["cssmin", "uglify", "cleanempty", "aws_s3:clean", "aws_s3:upload", "clean"]